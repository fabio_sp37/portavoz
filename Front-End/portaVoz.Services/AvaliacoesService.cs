﻿using portaVoz.Domain.Models;
using portaVoz.Domain.Repositories;
using portaVoz.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace portaVoz.Services
{
    public class AvaliacoesService : IAvaliacoesService
    {
        private readonly IAvaliacoesRepository _avaliacoesRepository;

        public AvaliacoesService(IAvaliacoesRepository avaliacoesRepository)
        {
            _avaliacoesRepository = avaliacoesRepository;
        }

        public async Task<IEnumerable<AvaliacaoMO>> List()
        {
            return await _avaliacoesRepository.List();
        }
    }
}
