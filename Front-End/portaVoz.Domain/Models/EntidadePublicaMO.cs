using System;
using System.Collections.Generic;

namespace portaVoz.Domain.Models
{
    public class EntidadePublicaMO
    {
        public int Id { get; set; }
        public string Nome { get; private set; }
        public string Bairro { get; private set; }
        public int IdTipoEntidade { get; private set; }
        public TipoEntidadeMO TipoEntidade { get; set; }
        public IList<AvaliacaoMO> Avaliacoes { get; set; } = new List<AvaliacaoMO>();
    }
}