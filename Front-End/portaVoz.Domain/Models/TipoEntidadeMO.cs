using System;
using System.Collections.Generic;

namespace portaVoz.Domain.Models
{
    public class TipoEntidadeMO
    {
        public int Id { get; private set; }
        public string Nome { get; private set; }
        public IList<EntidadePublicaMO> EntidadesPublicas { get; set; } = new List<EntidadePublicaMO>();
    }
}