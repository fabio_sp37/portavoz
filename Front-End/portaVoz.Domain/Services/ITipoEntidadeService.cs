﻿using portaVoz.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace portaVoz.Domain.Services
{
    public interface ITipoEntidadeService
    {
        Task<IEnumerable<TipoEntidadeMO>> List();
    }
}
