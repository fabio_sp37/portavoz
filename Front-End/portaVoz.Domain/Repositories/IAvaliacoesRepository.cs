﻿using portaVoz.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace portaVoz.Domain.Repositories
{
    public interface IAvaliacoesRepository
    {
        Task<IEnumerable<AvaliacaoMO>> List();
        Task Add(AvaliacaoMO avaliacao);
        Task FindById(int id);
        void Update(AvaliacaoMO avaliacao);
        void Remove(AvaliacaoMO avaliacao);
    }
}
