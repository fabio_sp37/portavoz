using System.Data;

namespace portaVoz.DAL
{
    public interface IOracleConnectionFactory
    {
        void Dispose();
        IDbConnection GetOpenConnection();
    }
}