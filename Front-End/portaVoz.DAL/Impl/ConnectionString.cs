using portaVoz.DAL.Domain.Models;
using portaVoz.DAL.Domain.Repository;
using Microsoft.Extensions.Configuration;

namespace portaVoz.DAL.Impl
{
    public class ConnectionString : IConnectionString
    {
        private readonly IConfigurationRoot configurationBuilded;

        public ConnectionString()
        {
            ConfigurationBuilder configuration = new ConfigurationBuilder();
            configuration.AddJsonFile("appsettings.json");
            configurationBuilded = configuration.Build();
        }

        public Conexao Conexao()
        {
            Conexao connections = new Conexao();
            IConfigurationSection section = configurationBuilded.GetSection("ConnectionStrings");
            section.Bind(connections);

            return connections;
        }
    }
}