﻿using portaVoz.DAL.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace portaVoz.DAL.Repositories
{
    public class BaseRepository
    {
        protected readonly IOracleConnectionFactory _oracleConnectionFactory;

        public BaseRepository(IOracleConnectionFactory oracleConnectionFactory) => _oracleConnectionFactory = oracleConnectionFactory;

    }
}
