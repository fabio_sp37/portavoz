﻿using portaVoz.Domain.Models;
using portaVoz.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;

namespace portaVoz.DAL.Repositories
{
    public class AvaliacoesRepository : BaseRepository, IAvaliacoesRepository
    {
        public AvaliacoesRepository(IOracleConnectionFactory connection) :base(connection)
        { }

        public Task Add(AvaliacaoMO avaliacao)
        {
            throw new NotImplementedException();
        }

        public Task FindById(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<AvaliacaoMO>> List()
        {
            IDbConnection connection = this._oracleConnectionFactory.GetOpenConnection();

            const string sql =
            @"
              SELECT ID_AVALIACAO as Id, 
                NM_AVALIADOR as NomeAvaliador, 
                IDADE_AVALIADOR as IdadeAvaliador, 
                AVALIADOR_RESIDENTE_LOCAL as AvaliadorResideLocal, 
                DT_AVALIACAO as DtAvaliacao, 
                TB_ENT_PUBLICA_ID_ENT as IdEntidade, 
                NOTA as Nota 
              FROM RM83369.TB_AVALIACAO
            ";

            var result = await connection.QueryAsync<AvaliacaoMO>(sql);
            
            return result.ToList();
        }

        public void Remove(AvaliacaoMO avaliacao)
        {
            throw new NotImplementedException();
        }

        public void Update(AvaliacaoMO avaliacao)
        {
            throw new NotImplementedException();
        }
    }
}
