using portaVoz.DAL.Domain.Models;

namespace portaVoz.DAL.Domain.Repository
{
    public interface IConnectionString
    {
        Conexao Conexao();
    }
}