using System;
using System.Collections.Generic;
using portaVoz.Models;
using System.Threading.Tasks;

namespace portaVoz.Business.Interfaces
{
    public interface IAvaliacaoBSS
    {
        int Adicionar(AvaliacaoMO avaliacao);
        AvaliacaoMO Obter(int idAvaliacao);
        IEnumerable<AvaliacaoMO> Listar();
        IEnumerable<AvaliacaoMO> ListarPorEntidade(int idEntidade);
        void Excluir(int idAvaliacao);
    }
}