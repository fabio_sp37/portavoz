using System;
using System.Collections.Generic;
using portaVoz.Models;

namespace portaVoz.Business.Interfaces
{
    public interface IEntidadePublicaBSS
    {
        int Adicionar(EntidadePublicaMO entidadePublica);
        EntidadePublicaMO Obter(int idEntidadePubica);
        IEnumerable<EntidadePublicaMO> Listar();
        IEnumerable<EntidadePublicaMO> ListarPorTipo(int idTipo);
        void Excluir(int idEntidadePubica);
    }
}