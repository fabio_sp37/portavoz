using System;
using System.Collections.Generic;
using portaVoz.Models;

namespace portaVoz.Business.Interfaces
{
    public interface ITipoEntidadeBSS
    {
        int Adicionar(TipoEntidadeMO tipoEntidade);
        IEnumerable<TipoEntidadeMO> Listar();
        TipoEntidadeMO Obter(int idTipo);
        void Excluir(int idTipo);
    }
}