using System;
using System.Collections.Generic;
using portaVoz.Business.Interfaces;
using portaVoz.Models;
using Dapper;
using portaVoz.DAL;
using System.Linq;
using System.Data;

namespace portaVoz.Business.Impl
{
    public class EntidadePublicaBSS : IEntidadePublicaBSS
    {
        private readonly IOracleConnectionFactory _oracleConnectionFactory;

        public EntidadePublicaBSS(IOracleConnectionFactory oracleConnectionFactory) => _oracleConnectionFactory = oracleConnectionFactory;
        public int Adicionar(EntidadePublicaMO entidadePublica)
        {
            _ = entidadePublica ?? throw new ArgumentNullException(nameof(entidadePublica));

            IDbConnection connection = this._oracleConnectionFactory.GetOpenConnection();
            const string sql =
            @"
              BEGIN
                  INSERT INTO RM83369.TB_ENTIDADE_PUBLICA (ID_ENTIDADE, NM_ENTIDADE, BAIRRO, TB_TIPO_ENT_ID_TIPO_ENT)
                  VALUES (
                    SQ_ENTIDADEPUBLICA.NEXTVAL, 
                    :Nome,
                    :Bairro,
                    :IdTipoEntidade
                  )
                  RETURNING ID_AVALIACAO into :t;
                  COMMIT;
              END; 
            ";
            var t = 0;
            int new_id = connection.Execute(sql, new
            {
                entidadePublica, t
            });

            if (new_id == 0)
            {
                throw new Exception("Falha ao inserir Tipo de Entidade");
            }

            return new_id;
        }

        public void Excluir(int idEntidadePubica)
        {
            IDbConnection connection = this._oracleConnectionFactory.GetOpenConnection();
            const string sql =
            @"DELETE FROM RM83369.TB_ENTIDADE_PUBLICA WHERE ID_ENTIDADE =: id";
            int new_id = connection.Execute(sql, new { id = idEntidadePubica });
            if (new_id == 0)
            {
                throw new Exception("Falha ao inserir Tipo de Entidade");
            }
        }

        public IEnumerable<EntidadePublicaMO> Listar()
        {
            IDbConnection connection = this._oracleConnectionFactory.GetOpenConnection();
            const string sql = 
            @"
              SELECT ID_ENTIDADE as Id, NM_ENTIDADE as Nome, BAIRRO as Bairro, TB_TIPO_ENT_ID_TIPO_ENT as IdTipoEntidade FROM RM83369.TB_ENTIDADE_PUBLICA
            ";

            List<EntidadePublicaMO> lista = connection.Query<EntidadePublicaMO>(sql).ToList();

            return lista;
        }

        public IEnumerable<EntidadePublicaMO> ListarPorTipo(int idTipo)
        {
            throw new NotImplementedException();
        }

        public EntidadePublicaMO Obter(int idEntidadePubica)
        {
            throw new NotImplementedException();
        }
    }
}