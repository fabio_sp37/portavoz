using System;
using System.Collections.Generic;
using portaVoz.Business.Interfaces;
using portaVoz.Models;
using Dapper;
using portaVoz.DAL;
using System.Linq;
using System.Data;
using System.Threading.Tasks;

namespace portaVoz.Business.Impl
{
    public class AvaliacaoBSS : IAvaliacaoBSS
    {
        private readonly IOracleConnectionFactory _oracleConnectionFactory;

        public AvaliacaoBSS(IOracleConnectionFactory oracleConnectionFactory) => _oracleConnectionFactory = oracleConnectionFactory;
        public int Adicionar(AvaliacaoMO avaliacao)
        {
            _ = avaliacao ?? throw new ArgumentNullException(nameof(avaliacao));

            IDbConnection connection = this._oracleConnectionFactory.GetOpenConnection();
            const string sql =
            @"
              BEGIN
                  INSERT INTO RM83369.TB_AVALIACAO (ID_AVALIACAO, NM_AVALIADOR, IDADE_AVALIADOR, AVALIADOR_RESIDENTE_LOCAL, DT_AVALIACAO, TB_ENT_PUBLICA_ID_ENT, NOTA)
                  VALUES (
                    SQ_AVALIACAO.NEXTVAL, 
                    :NomeAvaliador,
                    :IdadeAvaliador,
                    :AvaliadorResideLocal,
                    SYSDATE,
                    :IdEntidade,
                    :Nota
                  )
                  RETURNING ID_AVALIACAO into :t;
                  COMMIT;
              END; 
            ";
            var t = 0;
            int new_id = connection.Execute(sql, new { 
                avaliacao.NomeAvaliador, 
                avaliacao.IdadeAvaliador,
                avaliacao.AvaliadorResideLocal, 
                avaliacao.IdEntidade, 
                avaliacao.Nota, t });

            if (new_id == 0)
            {
                throw new Exception("Falha ao inserir Tipo de Entidade");
            }

            return new_id;
        }

        public void Excluir(int idAvaliacao)
        {

            IDbConnection connection = this._oracleConnectionFactory.GetOpenConnection();
            const string sql =
            @"DELETE FROM RM83369.TB_AVALIACAO WHERE ID_AVALIACAO =: id";
            int new_id = connection.Execute(sql, new { id = idAvaliacao });
            if (new_id == 0)
            {
                throw new Exception("Falha ao inserir Tipo de Entidade");
            }
        }

        public IEnumerable<AvaliacaoMO> Listar()
        {
            IDbConnection connection = this._oracleConnectionFactory.GetOpenConnection();
            
            const string sql = 
            @"
              SELECT ID_AVALIACAO as Id, 
                NM_AVALIADOR as NomeAvaliador, 
                IDADE_AVALIADOR as IdadeAvaliador, 
                AVALIADOR_RESIDENTE_LOCAL as AvaliadorResideLocal, 
                DT_AVALIACAO as DtAvaliacao, 
                TB_ENT_PUBLICA_ID_ENT as IdEntidade, 
                NOTA as Nota 
              FROM RM83369.TB_AVALIACAO
            ";
            
            return connection.Query<AvaliacaoMO>(sql);
        }

        public IEnumerable<AvaliacaoMO> ListarPorEntidade(int Entidade)
        {
            IDbConnection connection = this._oracleConnectionFactory.GetOpenConnection();
            List<AvaliacaoMO> lista = new List<AvaliacaoMO>();
            const string sql =
            @"
              SELECT ID_AVALIACAO as Id, 
                NM_AVALIADOR as NomeAvaliador, 
                IDADE_AVALIADOR as IdadeAvaliador, 
                AVALIADOR_RESIDENTE_LOCAL as AvaliadorResideLocal, 
                DT_AVALIACAO as DtAvaliacao, 
                TB_ENT_PUBLICA_ID_ENT as IdEntidade, 
                NOTA as Nota 
              FROM RM83369.TB_AVALIACAO
              WHERE TB_ENT_PUBLICA_ID_ENT = :Entidade
            ";

            lista = connection.Query<AvaliacaoMO>(sql, new { Entidade }).ToList();

            return lista;
        }

        public AvaliacaoMO Obter(int idAvaliacao)
        {
            throw new NotImplementedException();
        }
    }
}