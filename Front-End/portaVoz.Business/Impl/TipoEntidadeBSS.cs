using System;
using System.Collections.Generic;
using portaVoz.Business.Interfaces;
using portaVoz.Models;
using Dapper;
using portaVoz.DAL;
using System.Linq;
using System.Data;

namespace portaVoz.Business.Impl
{
    public class TipoEntidadeBSS : ITipoEntidadeBSS
    {
        private readonly IOracleConnectionFactory _oracleConnectionFactory;

        public TipoEntidadeBSS(IOracleConnectionFactory oracleConnectionFactory) => _oracleConnectionFactory = oracleConnectionFactory;

        public int Adicionar(TipoEntidadeMO tipoEntidade)
        {
            _ = tipoEntidade ?? throw new ArgumentNullException(nameof(tipoEntidade));

            IDbConnection connection = this._oracleConnectionFactory.GetOpenConnection();
            const string sql = 
            @"
              SET SERVEROUTPUT ON
              DECLARE temp tb_tipo_entidade.id_entidade%TYPE;
              BEGIN
              INSERT INTO TB_TIPO_ENTIDADE (ID_TIPO_ENTIDADE, NM_TIPO_ENTIDADE)
              VALUES (
                  SQ_TIPOENTIDADE.NEXTVAL, 
                  @Nome
              )
              RETURNING id_tipo into temp;
              COMMIT;
              DBMS_OUTPUT.put_line(temp);
              END; 
            ";

            int new_id = connection.QueryFirstOrDefault<int>(sql, tipoEntidade);

            if(new_id > 0)
            {
                throw new Exception("Falha ao inserir Tipo de Entidade");
            }

            return new_id;
        }

        public void Excluir(int idTipo)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TipoEntidadeMO> Listar()
        {
            IDbConnection connection = this._oracleConnectionFactory.GetOpenConnection();
            List<TipoEntidadeMO> lista = new List<TipoEntidadeMO>();
            const string sql = 
            @"
              SELECT ID_TIPO_ENTIDADE as Id, NM_TIPO_ENTIDADE as Nome FROM RM83369.TB_TIPO_ENTIDADE
            ";

            lista = connection.Query<TipoEntidadeMO>(sql).ToList();

            return lista;
        }

        public TipoEntidadeMO Obter(int idTipo)
        {
            throw new NotImplementedException();
        }
    }
}