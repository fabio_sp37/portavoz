import React from 'react'
import NavBar from '../Components/Default/NavBar'
import Container from '@material-ui/core/Container';

const Home = () => {
    return (
        <div>
            <NavBar />
            <Container fixed>
                <h1>Home</h1>
            </Container>
        </div>
    )
}

export default Home