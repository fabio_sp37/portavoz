import React, { useEffect } from 'react'
import NavBar from '../Components/Default/NavBar'
import Container from '@material-ui/core/Container';
import { requestAvaliacoesList } from '../Store/actions'
import { useDispatch, useSelector } from 'react-redux'

const Avaliacao = () => {
    const reducer = useSelector(r => r.avaliacoesListReducers)
    const lista = reducer.lista.length > 1? reducer.lista.map((avaliacao) => ({
        value: avaliacao.id, label: avaliacao.nota
    })) : []
    const dispatch = useDispatch()

    useEffect(() => {
        if(lista.length === 0) {
            dispatch(requestAvaliacoesList())
        }
    }, [dispatch, lista.length])

    return (
        <div>
            <NavBar />
            <pre>
                {JSON.stringify(lista)}
            </pre>
            <Container fixed>
                <h1>Avaliações</h1>
            </Container>
        </div>
    )
}

export default Avaliacao