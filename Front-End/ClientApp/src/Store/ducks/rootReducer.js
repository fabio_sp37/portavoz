import { combineReducers } from 'redux'
import avaliacoesListReducers from './avaliacoesListReducer'

export default combineReducers({
    avaliacoesListReducers
})