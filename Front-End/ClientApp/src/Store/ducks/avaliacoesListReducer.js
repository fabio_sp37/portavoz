import { createActions, createReducer } from 'reduxsauce'

//Actions types ans Creators
export const { Types, Creators } = createActions({
    avaliacoesListSuccess: ['lista', 'loading', 'err'],
    avaliacoesListRequest: ['lista', 'loading', 'err'],
    avaliacoesListFailure: ['lista', 'loading', 'err'],
    avaliacoesListLimpeza: ['lista']
})

//Reducer handlers
const INITIAL_STATE = {
    lista: [],
    loading: false,
    err: false
}

const avaliacoesListOk = (state = INITIAL_STATE, action) => (
    {
        lista: action.avaliacoes,
        loaging: false,
        err: false
    }
)

const avaliacoesListFail = (state = INITIAL_STATE, action) => (
    {
        lista: [],
        loading: false,
        err: true
    }
)

const avaliacoesListReq = (state = INITIAL_STATE, action) => (
    {
        lista: [],
        loading: true,
        err: false
    }
)

const avaliacoesListClr = (state = INITIAL_STATE, action) => (
    {
        lista: []
    }
)

//Reducer
export default createReducer(INITIAL_STATE, {
    [Types.AVALIACOES_LIST_SUCCESS]: avaliacoesListOk,
    [Types.AVALIACOES_LIST_REQUEST]: avaliacoesListReq,
    [Types.AVALIACOES_LIST_FAILURE]: avaliacoesListFail,
    [Types.AVALIACOES_LIST_LIMPEZA]: avaliacoesListClr
})