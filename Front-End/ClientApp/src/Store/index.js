import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootReducer from './ducks/rootReducer'

import rootSaga from './sagas'

const sagaMiddleware = createSagaMiddleware();

const configStore = (initialState = {}) => {
    const store = createStore(
        rootReducer,
        initialState,
        applyMiddleware(sagaMiddleware)
    )

    sagaMiddleware.run(rootSaga)

    return store
}

export default configStore
