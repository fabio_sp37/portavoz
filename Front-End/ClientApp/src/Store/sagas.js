import { takeLatest, put, call, all, delay } from 'redux-saga/effects'
import {
    AvaliacoesGetFetch
} from '../Services/portavoz'

function* avaliacoesList() {
    try {
        const response = yield call(() => AvaliacoesGetFetch())

        if(response.status !== 200) {
            throw response.status
        }

        const result = yield response.json()

        if(result !== null) {
            yield delay(1000)
            yield put(setAvaliacoesListOk(result))
        }
        else {
            throw new Error("Falha no fetch")
        }
    }
    catch(err) {
        console.log(err)
        yield put(setAvaliacoesListFail(true))
    }
}

export default function* root() {
    yield all([
        takeLatest('AVALIACOES_LIST_REQUEST', avaliacoesList)
    ]);
}

function setAvaliacoesListOk(avaliacoes) {
    return { type: 'AVALIACOES_LIST_SUCCESS', avaliacoes }
}

function setAvaliacoesListFail(err) {
    return { type: 'AVALIACOES_LIST_FAILURE', err }
}