const baseURL = 'https://localhost:44398/api/'

export const AvaliacoesGetFetch = () => {
    return fetch(`${baseURL}avaliacoes/listar`, {
        method: 'GET'
    })
}