import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import App from './App';
import configStore from './Store/index'
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <Provider store={configStore()}>
    <App></App>
  </Provider>,
  document.getElementById('root')
);

serviceWorker.register('/service-worker.js');
