import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import * as Scenes from '../Scenes/index'

const Routes = () => {
    return (
        <Router>
            <Switch>
                <Route path="/(home|)/" component={Scenes.Home} />
                <Route path="/avaliacoes" component={Scenes.Avaliacao} />
            </Switch>
        </Router>
    )
}

const RouteWithSubRoutes = (route) => {
    return (
        <Route
            path={route.path}
            render={props => 
                <route.component {...props} routes={route.routes} />
            } />
    )
}

export { RouteWithSubRoutes, Routes }