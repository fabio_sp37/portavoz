using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace portaVoz.Models
{
    public class AvaliacaoMO
    {
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Nome do Avaliador")]
        public string NomeAvaliador { get; set; }

        [Display(Name = "Idade do Avaliador")]
        public int IdadeAvaliador { get; set; }

        [Display(Name = "Data de Avaliação")]
        public DateTime DtAvaliacao { get; set; }

        [Display(Name = "Id Entidade")]
        public int IdEntidade { get; set; }

        [Display(Name = "Avaliador é residente local")]
        public string AvaliadorResideLocal { get; set; }

        [Display(Name = "Nota")]
        public int Nota { get; set; }
    }
}