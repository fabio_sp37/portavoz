using System;

namespace portaVoz.Models
{
    public class EntidadePublicaMO
    {
        public int Id { get; set; }
        public string Nome { get; private set; }
        public string Bairro { get; private set; }
        public int IdTipoEntidade { get; private set; }
    }
}