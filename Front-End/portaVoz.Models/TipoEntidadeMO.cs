using System;


namespace portaVoz.Models
{
    public class TipoEntidadeMO
    {
        public int Id { get; private set; }
        public string Nome { get; private set; }
    }
}