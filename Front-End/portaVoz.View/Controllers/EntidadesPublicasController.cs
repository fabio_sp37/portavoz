using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using portaVoz.Business.Impl;
using portaVoz.DAL;
using portaVoz.DAL.Domain.Repository;
using System;

namespace portaVoz.View.Controllers
{
    public class EntidadesPublicasController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConnectionString _configConnect;
        private EntidadePublicaBSS _business;

        public EntidadesPublicasController(ILogger<HomeController> logger, IConnectionString configConnect)
        {
            _= configConnect ?? throw new ArgumentNullException(nameof(configConnect));
            _configConnect = configConnect;

            using OracleConnectionFactory connectionFactoryFiapOn = new OracleConnectionFactory(_configConnect.Conexao().FiapOn);
            _logger = logger;
            _business = new EntidadePublicaBSS(connectionFactoryFiapOn);
        }

        public IActionResult Index()
        {
            var entidades = _business.Listar();
            return View(entidades);
        }

        public JsonResult Listar()
        {
            var entidades = _business.Listar();
            return Json(entidades);
        }
    }
}