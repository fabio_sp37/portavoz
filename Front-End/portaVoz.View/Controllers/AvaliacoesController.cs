using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using portaVoz.Business.Impl;
using portaVoz.DAL;
using portaVoz.DAL.Domain.Repository;
using System;
using portaVoz.Models;

namespace portaVoz.View.Controllers
{
    public class AvaliacoesController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConnectionString _configConnect;
        private AvaliacaoBSS _business;

        public AvaliacoesController(ILogger<HomeController> logger, IConnectionString configConnect)
        {
            _= configConnect ?? throw new ArgumentNullException(nameof(configConnect));
            _configConnect = configConnect;

            using OracleConnectionFactory connectionFactoryFiapOn = new OracleConnectionFactory(_configConnect.Conexao().FiapOn);
            _logger = logger;
            _business = new AvaliacaoBSS(connectionFactoryFiapOn);
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Listar(int idEntidade)
        {
            var avaliacoes = _business.ListarPorEntidade(idEntidade);
            return View("Index", avaliacoes);
        }

        public IActionResult Cadastrar()
        {
            return View();
        }

        [ActionName("Cadastro")]
        public IActionResult Cadastrar(AvaliacaoMO avaliacao)
        {
            avaliacao.AvaliadorResideLocal = avaliacao.AvaliadorResideLocal == "true" ? "S" : "N";
            _business.Adicionar(avaliacao);
            return View("Index");
        }

        public IActionResult Excluir(int id)
        {
            _business.Excluir(id);
            return View("Index");
        }

        public IActionResult Obter(int id)
        {
            throw new NotImplementedException();
        }
    }
}