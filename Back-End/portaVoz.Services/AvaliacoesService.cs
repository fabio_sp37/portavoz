﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using portaVoz.Domain.Models;
using portaVoz.Domain.Repositories;
using portaVoz.Domain.Services;

namespace portaVoz.Services
{
    public class AvaliacoesService : IAvaliacoesService
    {
        private readonly IAvaliacoesRepository _avaliacoesRepository;

        public AvaliacoesService(IAvaliacoesRepository avaliacoesRepository)
        {
            _avaliacoesRepository = avaliacoesRepository;
        }

        public void Delete(AvaliacaoMO avaliacao)
        {
            _avaliacoesRepository.Remove(avaliacao);
        }

        public async Task<AvaliacaoMO> FindById(int id)
        {
            return await _avaliacoesRepository.FindById(id);
        }

        public async Task<IEnumerable<AvaliacaoMO>> List()
        {
            return await _avaliacoesRepository.List();
        }
    }
}
