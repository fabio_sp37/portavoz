﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using portaVoz.Domain.Models;
using portaVoz.Domain.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace portaVoz.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AvaliacoesController : Controller
    {
        private readonly IAvaliacoesService _avaliacoesService;

        public AvaliacoesController(IAvaliacoesService avaliacoesService)
        {
            _avaliacoesService = avaliacoesService;
        }

        // GET: api/<controller>
        [HttpGet]
        [Route("listar")]
        public async Task<IEnumerable<AvaliacaoMO>> List()
        {
            var avaliacoes = await _avaliacoesService.List();
            return avaliacoes;
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete]
        [Route("excluir/{id}")]
        public async void Delete(int id)
        {
            var avaliacao = await _avaliacoesService.FindById(id);
            _avaliacoesService.Delete(avaliacao);
            
        }
    }
}
