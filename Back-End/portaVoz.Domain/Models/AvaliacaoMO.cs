using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace portaVoz.Domain.Models
{
    [Table("TB_AVALIACAO")]
    public class AvaliacaoMO
    {
        [Column("ID_AVALIACAO")]
        public int Id { get; set; }
        [Column("NM_AVALIADOR")]
        public string NomeAvaliador { get; set; }
        [Column("IDADE_AVALIADOR")]
        public string IdadeAvaliador { get; set; }
        [Column("DT_AVALIACAO")]
        public DateTime DtAvaliacao { get; set; }
        [Column("TB_ENT_PUBLICA_ID_ENT")]
        public int IdEntidade { get; set; }
        [Column("AVALIADOR_RESIDENTE_LOCAL")]
        public string AvaliadorResideLocal { get; set; }
        [Column("NOTA")]
        public int Nota { get; set; }
    }
}