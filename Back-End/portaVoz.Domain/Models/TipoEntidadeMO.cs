using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace portaVoz.Domain.Models
{
    [Table("TB_TIPO_ENTIDADE")]
    public class TipoEntidadeMO
    {
        [Column("ID_TIPO_ENTIDADE")]
        public int Id { get; private set; }
        [Column("NM_TIPO_ENTIDADE")]
        public string Nome { get; private set; }
    }
}