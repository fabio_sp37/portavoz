using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace portaVoz.Domain.Models
{
    [Table("TB_ENTIDADE_PUBLICA")]
    public class EntidadePublicaMO
    {
        [Column("ID_ENTIDADE")]
        public int Id { get; set; }
        [Column("NM_ENTIDADE")]
        public string Nome { get; private set; }
        [Column("BAIRRO")]
        public string Bairro { get; private set; }
        [Column("TB_TIPO_ENT_ID_TIPO_ENT")]
        public int IdTipoEntidade { get; private set; }
    }
}