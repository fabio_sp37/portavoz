﻿using portaVoz.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace portaVoz.Domain.Services
{
    public interface IAvaliacoesService
    {
        Task<IEnumerable<AvaliacaoMO>> List();
        void Delete(AvaliacaoMO avaliacao);
        Task<AvaliacaoMO> FindById(int id);
    }
}
