﻿using portaVoz.Persistence.Contexts;
using System;
using System.Collections.Generic;
using System.Text;

namespace portaVoz.Persistence.Repositories
{
    public class BaseRepository
    {
        protected readonly MainContext _context;

        public BaseRepository(MainContext context)
        {
            _context = context;
        }
    }
}
