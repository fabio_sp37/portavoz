﻿using Microsoft.EntityFrameworkCore;
using portaVoz.Domain.Models;
using portaVoz.Domain.Repositories;
using portaVoz.Persistence.Contexts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace portaVoz.Persistence.Repositories
{
    public class AvaliacoesRepository : BaseRepository, IAvaliacoesRepository
    {
        public AvaliacoesRepository(MainContext context) : base(context)
        { }

        public async Task Add(AvaliacaoMO avaliacao)
        {
            await _context.TB_AVALIACAO.AddAsync(avaliacao);
        }

        public async Task<AvaliacaoMO> FindById(int id)
        {
            return await _context.TB_AVALIACAO.FindAsync(id);
        }

        public async Task<IEnumerable<AvaliacaoMO>> List()
        {
            return await _context.TB_AVALIACAO.ToListAsync();
        }

        public void Remove(AvaliacaoMO avaliacao)
        {
            _context.TB_AVALIACAO.Remove(avaliacao);
            _context.SaveChanges();
        }

        public void Update(AvaliacaoMO avaliacao)
        {
            _context.TB_AVALIACAO.Update(avaliacao);
        }
    }
}
