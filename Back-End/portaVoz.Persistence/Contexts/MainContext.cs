﻿using System;
using Microsoft.EntityFrameworkCore;
using portaVoz.Domain.Models;

namespace portaVoz.Persistence.Contexts
{
    public class MainContext : DbContext
    {
        public MainContext(DbContextOptions<MainContext> options)
            : base(options)
        {
        }

        public DbSet<AvaliacaoMO> TB_AVALIACAO { get; set; }
        public DbSet<EntidadePublicaMO> TB_ENTIDADE_PUBLICA { get; set; }
        public DbSet<TipoEntidadeMO> TB_TIPO_ENTIDADE { get; set; }

    }
}
