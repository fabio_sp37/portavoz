### Features

- Adição / Listagem / Exclusão de avaliações;
- Utilização de framework Dapper micro ORM para conexão com banco remoto Oracle;
- Utilização Asp.Net Core MVC 3.1;
- Arquitetura de projeto em camadas;
- Factory de conexões com banco;
- Camada de negócios integrada;

# Readme.md



## Projeto criado com Dotnet Core 3.1

## Dotnet Core CLI

#### Após clonar o repositório acesse a pasta do projeto

`$ cd Front-End`

#### Limpe os arquivos de compilação

`$ dotnet clean`

#### Restaure os pacotes

`$ dotnet restore`

#### Compile o projeto

`$ dotnet build`

#### Execute 
`$ dotnet run -p portaVoz.View/portaVoz.View.csproj`